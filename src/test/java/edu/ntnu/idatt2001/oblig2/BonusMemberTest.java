package edu.ntnu.idatt2001.oblig2;

import edu.ntnu.idatt2001.oblig2.membershiptypes.SilverMembership;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class BonusMemberTest {

    @org.junit.jupiter.api.Test
    void registerBonusPointsBasicPositive() {
        BonusMember member = new BonusMember(1, LocalDate.now(), 5000, "Olsen, Ole", "ole@olsen.biz");
        member.registerBonusPoints(10000);
        assertEquals("Basic",
                member.getMembershipLevel());
    }
    @org.junit.jupiter.api.Test
    void registerBonusPointsBasicNegative() {
        BonusMember member = new BonusMember(1, LocalDate.now(), 30000, "Olsen, Ole", "ole@olsen.biz");
        member.registerBonusPoints(-10000);
        assertEquals("Basic",
                member.getMembershipLevel());
    }
    @org.junit.jupiter.api.Test
    void registerBonusPointsSilverPositive() {
        BonusMember member = new BonusMember(1, LocalDate.now(), 20000, "Olsen, Ole", "ole@olsen.biz");
        member.registerBonusPoints(10000);
        assertEquals("Silver",
                member.getMembershipLevel());
    }
    @org.junit.jupiter.api.Test
    void registerBonusPointsSilverNegative() {
        BonusMember member = new BonusMember(1, LocalDate.now(), 50000, "Olsen, Ole", "ole@olsen.biz");
        member.registerBonusPoints(-10000);
        assertEquals("Silver",
                member.getMembershipLevel());
    }
    @org.junit.jupiter.api.Test
    void registerBonusPointsGoldPositive() {
        BonusMember member = new BonusMember(1, LocalDate.now(), 70000, "Olsen, Ole", "ole@olsen.biz");
        member.registerBonusPoints(10000);
        assertEquals("Gold",
                member.getMembershipLevel());
    }
    @org.junit.jupiter.api.Test
    void registerBonusPointsGoldNegative() {
        BonusMember member = new BonusMember(1, LocalDate.now(), 90000, "Olsen, Ole", "ole@olsen.biz");
        member.registerBonusPoints(-10000);
        assertEquals("Gold",
                member.getMembershipLevel());
    }
}