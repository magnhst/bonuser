package edu.ntnu.idatt2001.oblig2;


/**
 * The Membership class is abstract and acts as a superclass of
 * the basic, silver and gold memberships classes. Includes a
 * methode for registering points and retrieving the membership
 * type name.
 */
public abstract class Membership {

    /**
     *  Calculates new bonusPointBalance and returns it
     * @param bonusPointBalance is current bonuspoinbalance
     * @param newPoints is the points to be added to the balance
     * @return new balance
     */
    public abstract int registerPoints(int bonusPointBalance, int newPoints);

    /**
     * Gets name of the membership
     * @return membership name
     */
    public abstract String getMembershipName();
}
