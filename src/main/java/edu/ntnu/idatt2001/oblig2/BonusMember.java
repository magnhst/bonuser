package edu.ntnu.idatt2001.oblig2;

import edu.ntnu.idatt2001.oblig2.membershiptypes.BasicMembership;
import edu.ntnu.idatt2001.oblig2.membershiptypes.GoldMembership;
import edu.ntnu.idatt2001.oblig2.membershiptypes.SilverMembership;

import java.time.LocalDate;

/**
 * BonusMember class describes a customer who has signed up for
 * the airline's bonus program.
 */
public class BonusMember {
    //These constants acts as point limits regarding membership level/type.
    //Having equal to or higher than the number of points described ensures
    //the customer getting that specific membership.
    private static final int SILVER_LIMIT = 25000;
    private static final int GOLD_LIMIT = 75000;

    private int memberNumber;
    private LocalDate enrolledDate;
    private int bonusPointsBalance = 0;
    private String name;
    private String eMailAddress;
    private String password;
    private Membership membership;

    /**
     * Creates new bonusMember object.
     * @param memberNumber The member number
     * @param enrolledDate Date of enrollment
     * @param bonusPointsBalance Number of points the member currently has
     * @param name Name of member
     * @param eMailAddress Email address of member
     */
    public BonusMember(int memberNumber, LocalDate enrolledDate, int bonusPointsBalance, String name, String eMailAddress) {
        this.memberNumber = memberNumber;
        this.enrolledDate = enrolledDate;
        this.bonusPointsBalance = bonusPointsBalance;
        this.name = name;
        this.eMailAddress = eMailAddress;
        checkAndSetMembership();
    }

    /**
     * Creates new bonusMember object.
     * @param memberNumber The member number
     * @param enrolledDate Date of enrollment
     * @param bonusPointsBalance Number of points the member currently has
     * @param name Name of member
     * @param eMailAddress Email address of member
     * @param password The password the member asks for.
     */
    public BonusMember(int memberNumber, LocalDate enrolledDate, int bonusPointsBalance, String name, String eMailAddress, String password) {
        this(memberNumber, enrolledDate, bonusPointsBalance, name, eMailAddress);
        this.password = password;
    }

    /**
     * Gets memberNumber
     * @return memberNumber
     */
    public int getMemberNumber() {
        return memberNumber;
    }

    /**
     * Gets enrolledDate
     * @return enrolledDate
     */
    public LocalDate getEnrolledDate() {
        return enrolledDate;
    }

    /**
     * Gets bonusPointsBalance
     * @return bonusPointsBalance
     */
    public int getBonusPointsBalance() {
        return bonusPointsBalance;
    }

    /**
     * Gets name
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets eMailAddress
     * @return eMailAddress
     */
    public String getEMailAddress() {
        return eMailAddress;
    }

    /**
     * Gets password
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Gets membership
     * @return membership
     */
    public Membership getMembership() {
        return membership;
    }

    /**
     * Password checker. Confirms that the password given by the user is correct/
     * fits the stored password.
     * @param password
     * @return {@code true} if the password match stored password,
     *         {@code false} if the password doesn't match.
     */
    public boolean checkPassword(String password){
        boolean success = false;
        if (this.password.equals(password)){
            success = true;
        } else {
            success = false;
        }
        return success;
    }

    /**
     * Registers/adds new points to a members bonusPointBalance.
     * @param newPoints The amount of points to add to the balance
     */
    public void registerBonusPoints(int newPoints){
        bonusPointsBalance = membership.registerPoints(bonusPointsBalance,newPoints);
        checkAndSetMembership();
    }

    /**
     * Gets the name of the membership-level and returns it.
     * @return membership-level name
     */
    public String getMembershipLevel(){
        return membership.getMembershipName();
    }

    /**
     * Checks which membership level the member should be in. Compares with the given
     * constant limits.
     */
    private void checkAndSetMembership(){
        if (bonusPointsBalance < SILVER_LIMIT) {
            membership = new BasicMembership();
        } else if (bonusPointsBalance >= SILVER_LIMIT && bonusPointsBalance < GOLD_LIMIT){
            membership = new SilverMembership();
        } else {
            membership = new GoldMembership();
        }
    }

    @Override
    public String toString() {
        return "BonusMember{" +
                "memberNumber=" + memberNumber +
                ", enrolledDate=" + enrolledDate +
                ", bonusPointsBalance=" + bonusPointsBalance +
                ", name='" + name + '\'' +
                ", eMailAddress='" + eMailAddress + '\'' +
                '}';
    }
}
