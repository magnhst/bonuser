package edu.ntnu.idatt2001.oblig2;

import java.time.LocalDate;
import java.util.HashMap;

/**
 * The member archive holds all the bonus members. The archive provides
 * functionality for adding members to the register, looking up bonus-points
 * of given members, registering new bonus-points and listing all the members.
 *
 * @author arne
 */
public class MemberArchive {

    // Use a HashMap, since the members have a unique member number.
    private HashMap<Integer, BonusMember> members;

    /**
     * Creates a new instance of MemberArchive.
     */
    public MemberArchive() {
        members = new HashMap<>();
        fillRegisterWithTestdata();
    }

    /**
     * Adds a new member to the register. The new member must have a member number
     * different from existing members. If not, the new member will not be added.
     *
     * @return {@code true} if the new member was added successfully,
     *         {@code false} if the new member could not be added, due to a
     *          member-number that already exists.
     */
    public boolean addMember(BonusMember bonusMember) {
        boolean success = false;
        if (!members.containsKey(members.size())){
            members.put(members.size(),bonusMember);
            success = true;
        }
        return success;
    }

    /**
     * Registers new bonus-points to the member with the member number given
     * by the parameter {@code memberNumber}. If no member in the register
     * matches the provided member number, {@code false} is returned.
     *
     * @param memberNumber the member number to add the bonus points to
     * @param bonusPoints the bonus points to be added
     * @return {@code true} if bonus-points were added successfully,
     *         {@code false} if not.
     */
    public boolean registerPoints(int memberNumber, int bonusPoints) {
        boolean success = false;
        if (!members.get(memberNumber).equals(null)){
            members.get(memberNumber).registerBonusPoints(bonusPoints);
            success = true;
        }
        return success;
    }

    /**
     * Finds a members points, given the member number exists in the archive
     * and the password is correct. If noe member in the register matches the
     * the provided member number a negative integer is returned.
     *
     * @param memberNumber the member number specifying which points to return
     * @param password the String password for confirming identity
     * @return memberBonusPointsBalance if the member is found/confirmed
     *          negative integer if member is not found or identity not confirmed
     */
    public int findPoints(int memberNumber, String password){
        int points=-1;
        if (!members.get(memberNumber).equals(null) && members.get(memberNumber).checkPassword(password)){
            points = members.get(memberNumber).getBonusPointsBalance();
        }
        return points;
    }

    /**
     * Lists all members to the console. Uses a toString methode
     */
    public void listAllMembers() {
        for (int i=1;i<=members.size();i++){
            System.out.println(members.get(i).toString());
        }
    }


    /**
     * Fills the register with some arbitrary members, for testing purposes.
     */
    private void fillRegisterWithTestdata() {
        BonusMember member = new BonusMember(1, LocalDate.now(), 10000, "Olsen, Ole", "ole@olsen.biz");
        members.put(member.getMemberNumber(), member);
        member = new BonusMember(2, LocalDate.now(), 15000, "Jensen, Jens", "jens@jensen.biz");
        members.put(member.getMemberNumber(), member);
        member = new BonusMember(3, LocalDate.now(), 5000, "Lie, Linda", "linda@lie.no");
        members.put(member.getMemberNumber(), member);
        member = new BonusMember(4, LocalDate.now(), 30000, "Paulsen, Paul", "paul@paulsen.org");
        members.put(member.getMemberNumber(), member);
        member = new BonusMember(5, LocalDate.now(), 75000, "FLo, Finn", "finn.flo@gmail.com");
        members.put(member.getMemberNumber(), member);

    }


}