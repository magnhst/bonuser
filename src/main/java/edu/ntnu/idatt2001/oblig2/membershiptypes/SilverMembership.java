package edu.ntnu.idatt2001.oblig2.membershiptypes;

import edu.ntnu.idatt2001.oblig2.Membership;

/**
 * Silver membership subclass of Membership
 */
public class SilverMembership extends Membership {
    private float POINTS_SCALING_FACTOR = 1.2f;
    private String membershipName = "Silver";

    @Override
    public int registerPoints(int bonusPointBalance, int newPoints){
        float newBonusPointBalance = bonusPointBalance + newPoints*POINTS_SCALING_FACTOR;
        return Math.round(newBonusPointBalance);
    }

    @Override
    public String getMembershipName() {
        return membershipName;
    }
}
