package edu.ntnu.idatt2001.oblig2.membershiptypes;

import edu.ntnu.idatt2001.oblig2.Membership;

/**
 * Gold membership subclass of Membership
 */
public class GoldMembership extends Membership {
    private float POINTS_SCALING_FACTOR_LEVEL1 = 1.3f;
    private float POINTS_SCALING_FACTOR_LEVEL2 = 1.5f;
    private String membershipName = "Gold";

    @Override
    public int registerPoints(int bonusPointBalance, int newPoints) {
        float newBonusPointBalance;
        if (bonusPointBalance < 90000){
            newBonusPointBalance = bonusPointBalance + newPoints*POINTS_SCALING_FACTOR_LEVEL1;
        } else {
            newBonusPointBalance = bonusPointBalance + newPoints*POINTS_SCALING_FACTOR_LEVEL2;
        }
        return Math.round(newBonusPointBalance);
    }

    @Override
    public String getMembershipName() {
        return membershipName;
    }
}
