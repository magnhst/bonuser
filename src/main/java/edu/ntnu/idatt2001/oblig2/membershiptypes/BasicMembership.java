package edu.ntnu.idatt2001.oblig2.membershiptypes;

import edu.ntnu.idatt2001.oblig2.Membership;

/**
 * Basic membership subclass of Membership
 */
public class BasicMembership extends Membership {
    private String membershipName = "Basic";

    @Override
    public int registerPoints(int bonusPointBalance, int newPoints){
        return bonusPointBalance + newPoints;
    }

    public String getMembershipName(){
        return membershipName;
    }
}
